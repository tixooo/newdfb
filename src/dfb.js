import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import React, { Component } from 'react';
import { Input, Select, Checkbox } from './components.js';



class DynamicForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {},
      showEmail: false
    };
  }

  handleChange = (name, value) => {
    this.setState((prevState) => ({
      values: {
        ...prevState.values,
        [name]: value
      }
    }));
    if (name === 'subscribe') {
      this.handleCheckboxChange(value);
    }
  };

  handleCheckboxChange = (value) => {
    if (value) {
      this.setState({
        showEmail: true
      });
    } else {
      this.setState({
        showEmail: false
      });
    }
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.values);
    this.setState({ values: {} });
  };
  renderElement = (element) => {
    const { type, id, label, required, placeholder, options } =
      element;
    const value = this.state.values[id] || '';
    switch (type) {
      case 'text':
        return (
          <Input
            key={id}
            label={label}
            required={required}
            placeholder={placeholder}
            value={value}
            onChange={(event) =>
              this.handleChange(id, event.target.value)
            }
          />
        );
      case 'select':
        return (
          <Select
            key={id}
            label={label}
            required={required}
            value={value}
            options={options}
            onChange={(event) =>
              this.handleChange(id, event.target.value)
            }
          />
        );
      case 'checkbox':
        return (
          <Checkbox
            key={id}
            label={label}
            required={required}
            value={value}
            onChange={(event) =>
              this.handleChange(id, event.target.checked)
            }
          />
        );
      default:
        return null;
    }
  };

  render() {
    const elements = this.props.formJSON.fields;
    return (
      <form onSubmit={this.handleSubmit}>
        {elements.map((element) => this.renderElement(element))}
        {this.state.showEmail && (
          <Input
            key="email"
            label="Email"
            required={true}
            placeholder="Please enter your email"
            value={this.state.values.email || ''}
            onChange={(event) =>
              this.handleChange('email', event.target.value)
            }
          />
        )}
        <button type="submit" className="btn btn-info text-light">
          Submit
        </button>
      </form>
    );
  }
}

DynamicForm.propTypes = {
  formJSON: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default DynamicForm;
