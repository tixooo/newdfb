import './App.css';
// eslint-disable-next-line
import React from 'react';
import formJSON from './formData.json';
import DynamicForm from './dfb.js';
import 'bootstrap/dist/css/bootstrap.min.css';

const submitHandler = (values) => {
    console.log(values);
};

let App = () => {
  return (
    <div className="container">
      <h1 className="text-bg-info text-md-center text-capitalize text-light">dynamic form builder</h1>
        <DynamicForm formJSON={formJSON} onSubmit={submitHandler} />
    </div>
  );
};
export default App;