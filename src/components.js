import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import React, { Component } from 'react';

class Input extends Component {
    render() {
        const { label, required, placeholder, value, onChange } =
            this.props;
        return (
            <div className="form-group">
                <label>
                    {label} {required && <span className="text-danger">*</span>}
                </label>
                <input
                    type="text"
                    className="form-control"
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                />
            </div>
        );
    }
}

class Select extends Component {
    render() {
        const { label, required, value, options, onChange } = this.props;
        return (
            <div className="form-group">
                <label>
                    {label} {required && <span className="text-danger">*</span>}
                </label>
                <select
                    className="form-control"
                    value={value}
                    onChange={onChange}
                >
                    {options.map((option) => (
                        <option
                            key={options.indexOf(option)}
                            value={option.value}
                        >
                            {option.label}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

class Checkbox extends Component {
    render() {
        const { label, required, value, onChange } = this.props;
        return (
            <div className="form-check">
                <input
                    type="checkbox"
                    className="form-check-input"
                    checked={value}
                    onChange={onChange}
                />
                <label className="form-check-label text-decoration-underline">
                    {label} {required && <span className="text-danger">*</span>}
                </label>
            </div>
        );
    }
}

    Input.propTypes = {
    label: PropTypes.string.isRequired,
    required: PropTypes.bool.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

    Select.propTypes = {
    label: PropTypes.string.isRequired,
    required: PropTypes.bool.isRequired,
    value: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
};

    Checkbox.propTypes = {
    label: PropTypes.string,
    required: PropTypes.bool,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func
};

    export { Input, Select, Checkbox };